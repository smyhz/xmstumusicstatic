<div align="center">
  <a href="https://music.xmedu.work/"><img height="100px" alt="logo" src="https://q1.qlogo.cn/g?b=qq&nk=2264596548&s=640"/></a>
  <p><em>🗂️音乐资源存储列表</em></p>
  <a href="https://smyhz.gitee.io/wx/zz/">
    <img src="https://img.shields.io/badge/%24-sponsor-ff69b4.svg" alt="赞助" />
  </a>
</div>

---


## 资源功能

- [x] 多种存储
    - [x] [阿里云对象存储oss-北京](https://cn.aliyun.com/)
    - [x] [阿里云对象存储oss-北京-2](https://cn.aliyun.com/)
- [x] 文件预览
- [x] 图像预览
- [x] 视频和音频预览，支持歌词和字幕
- [x] Office 文档预览（docx、pptx、xlsx、...）
- [x] 文件永久链接复制和直接文件下载
- [x] 资源打包下载
- [x] 允许访客上传，删除，新建文件夹，重命名，移动，复制


## 官网

<https://music.xmedu.work>

## 联系方式

> [@技术支持-QQ](http://wpa.qq.com/msgrd?v=3&uin=3079201190&site=qq&menu=yes)  ·  [@学生墙-QQ](http://wpa.qq.com/msgrd?v=3&uin=2264596548&site=qq&menu=yes)